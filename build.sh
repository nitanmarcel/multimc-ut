#!/bin/bash

set -e 

CLICK_ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)
CLICK_FRAMEWORK=ubuntu-sdk-16.04.5
ARCH="$1"


if [ $ARCH == "arm64" ]; then
    META_URL="https://raw.githubusercontent.com/theofficialgman/meta-multimc/master-clean/index.json"
fi

if [ $ARCH == "armhf" ]; then
    META_URL="https://raw.githubusercontent.com/theofficialgman/meta-multimc/master-clean-arm32/index.json"
fi

mkdir -p ${ROOT}/MultiMC && cd ${ROOT}/MultiMC
mkdir ${ROOT}/build
mkdir ${ROOT}/install

git clone --recursive https://github.com/MultiMC/Launcher ${ROOT}/src
cd ${ROOT}/build
cmake -DCMAKE_INSTALL_PREFIX=../install -DMultiMC_META_URL:STRING="$META_URL" ${ROOT}/src
make -j4 ${ROOT}/install

echo "$ARCH compatible binaries can be found in ${ROOT}/MultiMC"

cp ${ROOT}/manifest.json ${ROOT}/.clickable.build/install/manifest.json
sed -i "s/@CLICK_ARCH@/$CLICK_ARCH/g" ${ROOT}/.clickable.build/install/manifest.json
sed -i "s/@CLICK_FRAMEWORK@/$CLICK_FRAMEWORK/g" ${ROOT}/.clickable.build/install/manifest.json