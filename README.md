As of now this only builds the MultiMC binaries compatible with arm64 and arm32 devices.

Device setup required:


# OpenJDK needs to be installed on the device (You can also download archived openjdk versions online. But this is how I did it).

`sudo mount -o rw,remount /`
`sudo apt update`
`sudo apt install `openjdk-8-jdk`

# Copy the MultiMC folder and multimc.deskop file to your device

- Copy the MultiMC folder to `/home/phablet/.local/share/MultiMC`
  - The MultiMC folder needs to contain the `MultiMC` executable and a `bin` folder/
- Copy `multimc.desktop` file to `/home/phablet/.local/share/applications/multimc.desktop`

Done.

